package com.banco;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import com.exemplo.entidade.MenssagemAlunos;
import com.exemplo.entidade.MenssagemTurmas;

public class BancoAluno {


	public Configuration cfg;
	public SessionFactory sf;
	public Session session;
	public Transaction tx;

	public BancoAluno() {


		cfg = new AnnotationConfiguration();
		// Informe o arquivo XML que contém a configurações
		cfg.configure("/hibernate.cfg.xml");
		// Cria uma fábrica de sessões.
		// Deve existir apenas uma instância na aplicação
		sf = cfg.buildSessionFactory();
		// Abre sessão com o Hibernate
		session = sf.openSession();
		// Cria uma transação
		tx = session.beginTransaction();
	}


	//Método resposanvel por retornas todas as turmas que estao no banco

	public List<MenssagemAlunos> Listfrom(MenssagemTurmas usuario){

		String buscar="from MenssagemAlunos a where a.idturma like"+ "'"+usuario.getIdturma()+"'"+" order by a.area";
		List<MenssagemAlunos> list=new ArrayList<MenssagemAlunos>();
		list = session.createQuery(buscar).list();
	
		return list;
	}
	
	
	public List<MenssagemAlunos> ListfromWherearea(MenssagemAlunos usuario){

		String buscar="from MenssagemAlunos a where a.area like"+ "'"+usuario.getArea()+"'"+" order by a.nome";
		List<MenssagemAlunos> list=new ArrayList<MenssagemAlunos>();
		list = session.createQuery(buscar).list();
	    System.out.println("primeiro elemento:"+list.size());
		return list;
	}

	public List<MenssagemAlunos> ListfromWhereturno(MenssagemAlunos usuario){

		String buscar="from MenssagemAlunos a where a.turno like"+ "'"+usuario.getTurno()+"'"+" order by a.nome";
		List<MenssagemAlunos> list=new ArrayList<MenssagemAlunos>();
		list = session.createQuery(buscar).list();
	    System.out.println("primeiro elemento:"+list.size());
		return list;
	}

	
	
	
	public void insert(MenssagemAlunos aux){
		
		    session.clear();
			session.saveOrUpdate(aux);
			session.flush();
		
	}

	
	public void save(MenssagemAlunos aux){
		
		    session.clear();
			session.save(aux);
			session.flush();
		
	}
	
	public int Integrantes(MenssagemTurmas usuario) {

		String buscar = "from MenssagemAlunos a where a.idturma like " + "'"
				+ usuario.getIdturma() + "'";
		List<MenssagemAlunos> list = new ArrayList<MenssagemAlunos>();
		list = session.createQuery(buscar).list();
		if (list.isEmpty()) {
			return 0;

		}
		return list.size();
	}
	
	public void finalize(){

		tx.commit(); // Finaliza transação
		session.close(); // Fecha sessão
		sf.close();
	}

	public void delete(MenssagemAlunos aux){
		session.delete(aux);
	}

}
