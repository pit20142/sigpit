package com.banco;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import com.exemplo.entidade.MenssagemTurmas;

/*
 * Classe responsável por comunicar com o banco de dados
 * */

public class BancoTurmas {

	public Configuration cfg;
	public SessionFactory sf;
	public Session session;
	public Transaction tx;

	public BancoTurmas() {

		cfg = new AnnotationConfiguration();
		// Informe o arquivo XML que contém a configurações
		cfg.configure("/hibernate.cfg.xml");
		// Cria uma fábrica de sessões.
		// Deve existir apenas uma instância na aplicação
		sf = cfg.buildSessionFactory();
		// Abre sessão com o Hibernate
		session = sf.openSession();
		// Cria uma transação
		tx = session.beginTransaction();
	}

	// Método resposanvel por retornas todas as turmas que estao no banco

	public List<MenssagemTurmas> Listfrom() {
		String aux = " ";
		List<MenssagemTurmas> list = null;
		list = session.createQuery("from MenssagemTurmas").list();
		return list;
	}

	public void insert(MenssagemTurmas aux) {
	
			session.clear();
			session.saveOrUpdate(aux);
			session.flush();
		
	}
	
	public void save(MenssagemTurmas aux) {
		
		session.clear();
		session.save(aux);
		session.flush();
		
	}

	public void finalize() throws SQLException {

		tx.commit(); // Finaliza transação
		session.close(); // Fecha sessão
	    sf.close();  
	}

	public void remove(MenssagemTurmas aux) {
		session.delete(aux);
		session.flush();
	}
}
