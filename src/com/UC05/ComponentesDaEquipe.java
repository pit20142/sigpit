package com.UC05;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.banco.BancoAluno;
import com.banco.BancoTurmas;
import com.exemplo.entidade.MenssagemAlunos;
import com.exemplo.entidade.MenssagemTurmas;



//------ Autor: Felipe Gama ---------\\

/* Essa classe é responsável por listar as turmas cadastradas
*/

@ManagedBean(name="ComponentesBean")

public class ComponentesDaEquipe {
	
	
	//Variavel responsável por receber a consulta ao banco \\
	private List<MenssagemTurmas> TurmaLista=new ArrayList<MenssagemTurmas>();
	// Variaveis responsavel por comunicar entre as janelas e excluir ou adicionar turmas \\
	private String idturma;
	private String nome;
	private Integer ano;
	private Integer semestre;
	private String projeto;
	private String ativo;
	private String area;
	private String turno;
	
	List<MenssagemAlunos> AlunosLista = new ArrayList<MenssagemAlunos>();
	
	
	
	
	
	
	//metodo responsavel por retornar o banco de turmas \\ 
	// Tambem ordena a lista por ano e depois por semestre \\


	public String getTurno() {
		return turno;
	}


	public void setTurno(String turno) {
		this.turno = turno;
	}


	public List<MenssagemTurmas> getTurmaLista() {
		return TurmaLista;
	}


	public void setTurmaLista(List<MenssagemTurmas> turmaLista) {
		TurmaLista = turmaLista;
	}


	public List<MenssagemAlunos> getAlunosLista() {
		return AlunosLista;
	}


	public void setAlunosLista(List<MenssagemAlunos> alunosLista) {
		AlunosLista = alunosLista;
	}


	public String getArea() {
		return area;
	}


	public void setArea(String area) {
		this.area = area;
	}


	public String getIdturma() {
		return idturma;
	}


	public void setIdturma(String idturma) {
		this.idturma = idturma;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public Integer getAno() {
		return ano;
	}


	public void setAno(Integer ano) {
		this.ano = ano;
	}


	public Integer getSemestre() {
		return semestre;
	}


	public void setSemestre(Integer semestre) {
		this.semestre = semestre;
	}


	public String getProjeto() {
		return projeto;
	}


	public void setProjeto(String projeto) {
		this.projeto = projeto;
	}


	public String getAtivo() {
		return ativo;
	}


	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}


	public List<MenssagemTurmas> getConsultaTurma() {
		BancoTurmas turmas=new BancoTurmas();
		TurmaLista=turmas.Listfrom();
		Collections.sort(TurmaLista);
		try {
			turmas.finalize();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return TurmaLista;
	}
	
	
	//metodo responsavel por chamar a pagina para exibição de uma turma expecifica \\

		public String Turma(MenssagemTurmas aux) {
			this.idturma=aux.getIdturma();
			this.nome=aux.getNome();
			this.ano=aux.getAno();
			this.semestre=aux.getSemestre();
			this.projeto=aux.getProjeto();
			getConsultaAluno();
			return "componentesturmas";
		}
		
		
		public String Buscaralunoarea(){
			
			getConsultaAlunoWherearea();
			return "componentesturmas";
		}
		
		public String BuscarAlunoTurno(){
			
			getConsultaAlunoWhereTurno();
			return "componentesturmas";
		}
		
		public void getConsultaAlunoWherearea(){
			MenssagemAlunos aux= new MenssagemAlunos();
            aux.setArea(area); 
            System.out.println("Área: "+area);
 			BancoAluno turmas=new BancoAluno();
			AlunosLista=turmas.ListfromWherearea(aux);
			//Collections.sort(AlunosLista);
			turmas.finalize();

			
			
		}
		
		
		public void getConsultaAlunoWhereTurno(){
			MenssagemAlunos aux= new MenssagemAlunos();
            aux.setTurno(turno);
 			BancoAluno turmas=new BancoAluno();
			AlunosLista=turmas.ListfromWhereturno(aux);
			//Collections.sort(AlunosLista);
			turmas.finalize();
			
		}
		
		public void getConsultaAluno() {
			MenssagemTurmas aux= new MenssagemTurmas();
			aux.setAno(ano);
			aux.setIdturma(idturma);
			aux.setNome(nome);
			aux.setSemestre(semestre);
			BancoAluno turmas=new BancoAluno();
			AlunosLista=turmas.Listfrom(aux);
			//Collections.sort(AlunosLista);
			turmas.finalize();

			
		}


	

}
