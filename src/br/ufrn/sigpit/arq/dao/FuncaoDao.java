package br.ufrn.sigpit.arq.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.ufrn.sigpit.arq.dominio.Funcao;
import br.ufrn.sigpit.arq.exceptions.DAOException;

/**
 * Classe centralizadora das consultas envolvendo a funcao.
 * 
 * @author Mario Melo
 *
 */
@Repository
public class FuncaoDao extends GenericDao{
	
	@Autowired
	public FuncaoDao(SessionFactory sessionFactory){
		setSessionFactory(sessionFactory);
	}
	
	/**
	 * Metodo utilizado para buscar uma funcao pelo nome.
	 * 
	 * @param nome
	 * @return
	 */
	public Funcao findFuncaoByName(String nome){
		try{
			Criteria c = getCriteria(Funcao.class);
			
			c.add(Restrictions.like("nome", nome));
			
			return (Funcao) c.uniqueResult();
		}catch(Exception e){
			throw new DAOException ("Erro ao listar função por nome no DAO.");
		}
	}
	
	/**
	 * Metodo utilizado para remover uma funcao do sistema.
	 * 
	 * @param Funcao
	 * @return
	 */
	public void delete(Funcao f) throws DAOException{
		try {
			getSession().delete(f);
		} catch (Exception e) {
			throw new DAOException ("Erro ao deletar função no DAO.");
		}
	}
	
	/**
	 * Metodo utilizado para listar todas as funcoes cadastradas no sistema.
	 * 
	 * @return List
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	public List<Funcao> listAll(){
		try{
			Criteria c = getCriteria(Funcao.class);
			List<Funcao> lista = c.list();
			return c.list();
		} catch(Exception e){
			throw new DAOException ("Erro ao listar todas as funções no DAO.");
		}
	}

}
