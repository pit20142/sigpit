
package br.ufrn.sigpit.arq.jsf.usuario;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.ufrn.sigpit.arq.jsf.AbstractMBean;
import br.ufrn.sigpit.arq.seguranca.dominio.Usuario;


@ManagedBean
@RequestScoped
@Component
@Scope(value="request")
public class BuscarUsuarioMBean extends AbstractMBean<Usuario> {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public BuscarUsuarioMBean(){
		
	}

}
	
