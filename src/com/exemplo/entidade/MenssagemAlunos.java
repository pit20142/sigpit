package com.exemplo.entidade;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="alunosfinal", schema="public")
public class MenssagemAlunos {

	@Id 
	//@GeneratedValue(strategy = GenerationType.IDENTITY) 
	//@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="\"email\"") 
	private String email;
	private String nome;
	private String turno;
	private String area;
	private String idturma;
	
	
	
	public String getNome() {
		return nome;
	}



	public void setNome(String nome) {
		this.nome = nome;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getTurno() {
		return turno;
	}



	public void setTurno(String turno) {
		this.turno = turno;
	}



	public String getArea() {
		return area;
	}



	public void setArea(String area) {
		this.area = area;
	}



	public String getIdturma() {
		return idturma;
	}



	public void setIdturma(String idturma) {
		this.idturma = idturma;
	}



	
	
	
	
	
}
