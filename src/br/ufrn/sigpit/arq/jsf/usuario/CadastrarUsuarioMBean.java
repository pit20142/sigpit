package br.ufrn.sigpit.arq.jsf.usuario;

import java.util.Date;
import java.util.Properties;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import br.ufrn.sigpit.arq.dao.GenericDao;
import br.ufrn.sigpit.arq.dominio.DadosPessoais;
import br.ufrn.sigpit.arq.jsf.AbstractMBean;
import br.ufrn.sigpit.arq.seguranca.dao.UsuarioDao;
import br.ufrn.sigpit.arq.seguranca.dominio.Usuario;
import br.ufrn.sigpit.arq.service.ServiceCadastro;
import br.ufrn.sigpit.comum.helpers.AutenticacaoHelper;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.servlet.http.HttpServletRequest;

@ManagedBean
@RequestScoped
@Component
@Scope(value="request")
public class CadastrarUsuarioMBean extends AbstractMBean<Usuario>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private DadosPessoais dadosp;
	private String senhaConfirm;
	

	public String getSenhaConfirm() {
		return senhaConfirm;
	}

	public void setSenhaConfirm(String senhaConfirm) {
		this.senhaConfirm = senhaConfirm;
	}
	
	@Autowired
	private ServiceCadastro serviceCadastro;
	
	public CadastrarUsuarioMBean(){
		obj = new Usuario();
	}
	
	public String cadastrarDadosAcesso(){
		
		//obj = new Usuario();

		//dadosp = new DadosPessoais(nome, null, sexo);

		obj.setDadosPessoais(dadosp);		
		//obj.setEmail(email);
		//obj.setSenha(AutenticacaoHelper.toMD53(senha));
		//obj.setSenha(senha);
		obj.setChaveConfirmacao(AutenticacaoHelper.gerarChaveConfirmacao());
		
		
		try {
			
			serviceCadastro.salvar(obj);
	        
			sendEmailConfirmation();
	        
	        addFacesInformationMessage("Seu cadastro foi efetuado com sucesso. Ative já sua conta através do email "+obj.getEmail()+".\n  Se o e-mail não estiver em sua caixa de entrada, verifique sua caixa de spam!" );
	        addFacesInformationMessage("Caso não tenha recebido o email de ativação, clique <a href='" + getCurrentRequest().getContextPath() + "/reenviarEmailAtivacao.jsf'>AQUI</a>." );
		}
		catch(DataIntegrityViolationException c){
			c.printStackTrace();
			addGlobalErrorMessage("Já Existe uma conta associada aos seus dados, por favor faça login");
			return "login.jsf";
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "EmailConfirmacao.jsf";
		
	}
	public void sendEmailConfirmation() throws AddressException, MessagingException{
		  
		  Properties props = new Properties();  
		  props.put("mail.smtp.host","mail.info.ufrn.br");
		  props.put("mail.smtp.port", "25"); 
		 
		  Session session = Session.getDefaultInstance(props);  
		 
		  MimeMessage message = new MimeMessage(session);  	  
		  
		  Address from = new InternetAddress("ticonnectsinfo@gmail.com");  
		  Address to = new InternetAddress(obj.getEmail());  
		   
		  message.setFrom(from);  
		  message.addRecipient(RecipientType.TO, to);  
		  
		  message.setSentDate(new Date());  
		  message.setSubject("Confirmação de Cadastro no SiGPIT");  
		  
		  //Obtendo nome, porta e contexto do servidor
		  String enderecoServidor,porta, contexto;
		  Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();

		  enderecoServidor = ((HttpServletRequest) request).getLocalName().toString();
		  contexto = ((HttpServletRequest) request).getContextPath().toString();
		  porta = Integer.toString(((HttpServletRequest) request).getServerPort());

		  
		  String html = "<p><h2>Olá "+obj.getNome()+", </h2></p> ";
		  html += "<p><h4>Seja bem-vindo(a) ao SIGS!</h4></p>";
		  html += "<p><h4>Atenção: sua conta encontra-se desativada. Ative já a sua conta clicando <a href='http://"+enderecoServidor+":"+porta+contexto+"/login.jsf?chave="+obj.getChaveConfirmacao()+"'>AQUI</a>.</h4></p>";
		  html += "<p></br><h4>Atenciosamente,</h4></p>";
		  html += "<p><h4>SiGPIT - Sistema de Gerenciamento do Programa de Imerssão Tecnológica</h4></p>";
		  
		  message.setText(html,"ISO-8859-1", "html");  		  
		
		  Transport.send(message);  
		}
	
public String getMensagemConfirmacaoConta() {
		
		String chave = getParameter("chave");
		
//		System.out.println("CadastroMBean.geetMensagemConfirmacaoConta - getParameter" + chave);
		
		if (chave != null){
			UsuarioDao dao = (UsuarioDao) getBean("usuarioDao");
			
			if (dao.isChaveCadastrada(chave)){
				
				obj = dao.findUsuarioByChaveConfirmacao(chave);
				
				if (!obj.isAtivo()){
				
					obj.setAtivo(true);
					
					serviceCadastro.atualizar(obj);
				}
				addFacesInformationMessage("Conta ativada com sucesso! Efetue login no sistema:");
				
				/*
				// A primeira vez que o Gestor ativa precisa redirecionar para um cadastro de empresa 
				for (Permissao p : obj.getPermissoes()) {
					if(p.getPapel().getNome().equals("ROLE_GERENTE")) cadastroGestor = true;
				}
				if(cadastroGestor){
					addFacesInformationMessage("Conta ativada com sucesso! Efetue login no sistema:");
				} else addFacesInformationMessage("Conta ativada com sucesso!");
				
				*/
			} else {
				addGlobalErrorMessage("A conta já foi ativada anteriormente ou o código de acesso não é válido.");
			}
		}
		
		return "";
		
	}
/*
	public String deletarUsuario(){
		System.out.println("Cheguei no deletar e o id: " + obj.getId());
		//deletar(obj.getId());
		return "/login.jsf";	
	}
*/
	public String deletar() {
		//System.out.println("Cheguei no deletar e o id: " + obj.getId());
		GenericDao dao = (GenericDao) getBean("genericDao");
		Usuario user = dao.buscar(Usuario.class, getUsuarioLogado().getId());
		obj = user;
		
		try {
			System.out.println("Cheguei no excluir");
			serviceCadastro.excluir(obj);
		} catch (Exception e) {
			addGlobalErrorMessage("Não foi possível excluir o Usuário.");
		}
		return "/ConfirmExcluirConta.jsf";
	}
}
