package com.UC09;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.hibernate.mapping.Collection;

import com.banco.BancoAluno;
import com.banco.BancoTurmas;
import com.exemplo.entidade.MenssagemAlunos;
import com.exemplo.entidade.MenssagemTurmas;
import com.sun.faces.context.flash.ELFlash;




//------ Autor: Felipe Gama ---------\\

/* Essa classe é responsável por listar as turmas cadastradas
 */

@ManagedBean(name="CadastroBean")
@SessionScoped
public class ManterTurmas{

	/**
	 * 
	 */

	//Variavel responsável por receber a consulta ao banco \\
	private List<MenssagemTurmas> TurmaLista=new ArrayList<MenssagemTurmas>();
	// Variaveis responsavel por comunicar entre as janelas e excluir ou adicionar turmas \\
	private String idturma;
	private String nome;
	private Integer ano;
	private Integer semestre;
	private String projeto;
	private String ativo;



	// variaveis por mapear aluno \\

	private String nomealuno;
	private String email;
	private String turno;
	private String area;




	public String getNomealuno() {
		return nomealuno;
	}


	public void setNomealuno(String nomealuno) {
		this.nomealuno = nomealuno;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getTurno() {
		return turno;
	}


	public void setTurno(String turno) {
		this.turno = turno;
	}


	public String getArea() {
		return area;
	}


	public void setArea(String area) {
		this.area = area;
	}



	private MenssagemAlunos aluno =new MenssagemAlunos();


	private String procurar;



	public String getProcurar() {
		return procurar;
	}


	public void setProcurar(String procurar) {
		this.procurar = procurar;
	}


	public String getProjeto() {
		return projeto;
	}


	public void setProjeto(String projeto) {
		this.projeto = projeto;
	}


	public String getAtivo() {
		return ativo;
	}


	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}


	public MenssagemAlunos getAluno() {
		return aluno;
	}


	public void setAluno(MenssagemAlunos aluno) {
		this.aluno = aluno;
	}



	public List<MenssagemTurmas> getTurmaLista() {
		return TurmaLista;
	}


	public void setTurmaLista(List<MenssagemTurmas> turmaLista) {
		TurmaLista = turmaLista;
	}








	public String getIdturma() {
		return idturma;
	}


	public void setIdturma(String idturma) {
		this.idturma = idturma;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public Integer getAno() {
		return ano;
	}


	public void setAno(Integer ano) {
		this.ano = ano;
	}


	public Integer getSemestre() {
		return semestre;
	}


	public void setSemestre(Integer semestre) {
		this.semestre = semestre;
	}


	//metodo responsavel por retornar o banco de turmas \\ 
	// Tambem ordena a lista por ano e depois por semestre \\

	public List<MenssagemTurmas> getConsultaTurma() {
		BancoTurmas turmas=new BancoTurmas();
		TurmaLista=turmas.Listfrom();
		Collections.sort(TurmaLista);
		try {
			turmas.finalize();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return TurmaLista;
	}


	//metodo responsavel por chamar a pagina para exibição de uma turma expecifica \\

	public String Turma(MenssagemTurmas aux) {
		this.idturma=aux.getIdturma();
		this.nome=aux.getNome();
		this.ano=aux.getAno();
		this.semestre=aux.getSemestre();
		this.projeto=aux.getProjeto();
		return "turmas";
	}

	public String AddTurmaChamar(){

		return "adicionarturma";
	}

	public void EditarTurma(MenssagemTurmas aux) {
		this.idturma=aux.getIdturma();
		this.nome=aux.getNome();
		this.ano=aux.getAno();
		this.semestre=aux.getSemestre();
		this.ativo=aux.getAtivo();
	}

	// metodo responsavel por atualizar o banco de dados \\
	public String Cadastro(){
		MenssagemTurmas msg=new MenssagemTurmas();
		FacesMessage msgerro = new FacesMessage("Dados insuficientes! ou turma já cadastrada");
		try{

			if(nome.equals("") || ano==0 || semestre==0){
				FacesContext.getCurrentInstance().addMessage("erroturma", msgerro);
			}else{
				msg.setNome(nome);
				msg.setAno(ano);
				msg.setSemestre(semestre);
				msg.setIdturma(ano+"."+semestre+"."+nome);
				msg.setAtivo("ativo");
				BancoTurmas turmas=new BancoTurmas();
				turmas.save(msg); // inserir o novo cadastro
				turmas.finalize();
				return "buscarturmas";
			}

		}catch(Exception e){
			FacesContext.getCurrentInstance().addMessage("erroturma", msgerro);
		}



		return "";
	}




	public List<MenssagemAlunos> getConsultaAluno() {
		MenssagemTurmas aux= new MenssagemTurmas();
		aux.setAno(ano);
		aux.setIdturma(idturma);
		aux.setNome(nome);
		aux.setSemestre(semestre);
		List<MenssagemAlunos> AlunosLista = new ArrayList<MenssagemAlunos>();
		BancoAluno turmas=new BancoAluno();
		AlunosLista=turmas.Listfrom(aux);
		//Collections.sort(AlunosLista);
		turmas.finalize();

		return AlunosLista;
	}


	// metodo responsavel por remover a turma do banco de dados \\
	public String Exclusao(MenssagemTurmas msg){

		BancoTurmas turmas=new BancoTurmas();
		turmas.remove(msg); // remover o cadastro da turma aux
		System.out.println("Estou apagando o: "+msg.getNome());
		try {
			turmas.finalize();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // Toda vez que se comunicar tem que finalizar a mesma
		return "buscarturmas?faces-redirect=true";
	}

	public String Chamar_add_aluno(){
		return "addaluno";
	}

	public String Cadastro_aluno(){

		FacesMessage msg = new FacesMessage("Usuário ou senha inválido! ou aluno cadastrado");
		try{
			if(nomealuno.equals("") || email.equals("") || area.equals("") || idturma.equals(" ")){
				FacesContext.getCurrentInstance().addMessage("erroturma", msg);
			}else{
				System.out.println("Matricula da Turma: "+idturma);
				System.out.println("Nome do aluno: "+nomealuno);
				System.out.println("Area do aluno: "+area);
				System.out.println("Email do aluno: "+email);
				MenssagemAlunos aluno = new MenssagemAlunos();
				BancoAluno bancoaluno=new BancoAluno();
				aluno.setNome(nomealuno);
				aluno.setEmail(email);
				aluno.setArea(area);
				aluno.setTurno(turno);
				aluno.setIdturma(idturma);
				bancoaluno.save(aluno);
				bancoaluno.finalize();
				return "turmas";
			}
		}catch(Exception e1){
			FacesContext.getCurrentInstance().addMessage("msgErro", msg);
		}
		FacesContext.getCurrentInstance().addMessage("msgErro", msg);
		return "";
	}

	// Metodo Responsavel por Editar uma turma \\
	public String EditarTurmaFinal() {
		MenssagemTurmas aux = new MenssagemTurmas();
		FacesMessage msgerro = new FacesMessage("Dados insuficientes! ou turma já cadastrada");
		try{

			if(nome.equals("") || ano==0 || semestre==0){
				FacesContext.getCurrentInstance().addMessage("erroturma", msgerro);
			}else{
				aux.setIdturma(idturma);
				aux.setNome(nome);
				aux.setAno(ano);
				aux.setSemestre(semestre);
				aux.setProjeto(projeto);
				aux.setAtivo(ativo);
				BancoTurmas turmas = new BancoTurmas();		//Cria uma conexao com o banco Turmas
				turmas.insert(aux);		//Atualiza a turma especifica
				turmas.finalize();	//Finaliza a conexao com o banco Turma
				return "buscarturmas";
			}
		}catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("erroturma", msgerro);
		}
		return "";
	}


	public void Cadastro_Projeto(){
		MenssagemTurmas turma =new MenssagemTurmas();
		turma.setAno(ano);
		turma.setSemestre(semestre);
		turma.setIdturma(idturma);
		turma.setNome(nome);
		turma.setProjeto(projeto);
		turma.setAtivo(ativo);
		BancoTurmas bancoturmas=new BancoTurmas();
		bancoturmas.insert(turma);
		System.out.println("CADASTROU "+idturma+"PROJETO "+projeto);
		try {
			bancoturmas.finalize();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// metodo responsavel por retornar o numero de cadastrados alunos na turma \\
	public int numalunos(MenssagemTurmas aux){
		BancoAluno Alunos = new BancoAluno();	//Cria conexao com o banco Aluno
		return Alunos.Integrantes(aux);
	}

	public String ChamarEditarTurma(){

		return "editarturma";
	}

	public void Pesquisar_Turmas(){
		List<MenssagemTurmas> pesquisa_turma= new ArrayList<MenssagemTurmas>();
		for (MenssagemTurmas turma: TurmaLista){
			if(turma.getNome().equals(procurar)){
				pesquisa_turma.add(turma);
				System.out.println("Nome: "+turma.getNome());
			}
		} 


	}
}
