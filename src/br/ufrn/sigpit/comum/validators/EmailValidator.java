/**
 * Universidade Federal do Rio Grande do Norte
 * Superintend�ncia de Inform�tica - UFRN
 * Diretoria de Sistemas
 *
 * Created on 06/06/2012
 *
 */
package br.ufrn.sigpit.comum.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Validator responsavel pela validacao do campo email no cadastro dos dados de
 * acesso. Checa-se se o e-mail esta no formato abc@abc.com, por exemplo. A
 * expressao regular utilizada para realizar essa validacao foi obtida de
 * http://www.regular-expressions.info/email.html.
 * 
 * @author Argus Halley
 */
@FacesValidator(value = "br.ufrn.sigpit.comum.validators.EmailValidator")
public class EmailValidator implements Validator {
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		String email = (String) value;

		Pattern pattern = Pattern
				.compile("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
		Matcher matcher = pattern.matcher(email);

		if (email != null && !matcher.find()) {
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"Insira um email válido!", null));
		}
	}
}
