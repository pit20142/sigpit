package br.ufrn.sigpit.arq.seguranca.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufrn.sigpit.arq.seguranca.dominio.Usuario;
import br.ufrn.sigpit.arq.service.AbstractService;


@Service
public class ServiceLogon extends AbstractService{


	@Autowired
	private AuthenticationManager authenticationManager; // specific for Spring Security
	

	@Transactional(readOnly=true)
	public Usuario login(Usuario usuario){
		try {
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(usuario.getEmail(), usuario.getSenha());
			System.out.println("Token criado");
			
			Authentication authenticate = authenticationManager.authenticate(token);
			if (authenticate.isAuthenticated()) {
				SecurityContextHolder.getContext().setAuthentication(authenticate);
				Usuario user = (Usuario) authenticate.getPrincipal();
				return user;
			}
		} catch (AuthenticationException e) {
			throw e;
		}
		return null;
	}
	

	public void logout() {
		SecurityContextHolder.getContext().setAuthentication(null);
	}
	
}
