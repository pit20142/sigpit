package br.ufrn.sigpit.arq.dao;

import java.lang.reflect.AnnotatedElement;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

public class ArqUtils{
	
	public static final String getBeanName(Class<?> beanClass ){
		
		AnnotatedElement annoted = beanClass;
		
		Component component = annoted.getAnnotation(Component.class);
		
		String beanName = component.value();
		if (beanName == null || beanName.trim().equals("")) {
			beanName = StringUtils.uncapitalize(beanClass.getSimpleName());
		}
		
		return beanName;
	}
}
