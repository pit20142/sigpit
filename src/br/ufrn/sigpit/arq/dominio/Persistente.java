package br.ufrn.sigpit.arq.dominio;

import java.io.Serializable;

/**
 * Implementa validacao nas classes de dominio.
 * 
 * @author Mario Melo
 *
 */
public interface Persistente extends Serializable {

	
	public int getId();
	
	public void setId(int id);
	
	
}
