package br.ufrn.sigpit.arq.dao;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * F�brica de Daos.
 * 
 * @author M�rio Melo
 *
 */
public class DaoFactory {
	protected static DaoFactory singleton = new DaoFactory();

	public static DaoFactory getInstance() {
		return singleton;
	}
	
	private DaoFactory() {
		// Construtor privado para n�o permitir instancia��o
	}
	
	/**
	 * Retorna um dao a partir de sua classe.
	 * 
	 * @param daoName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T extends GenericDao> T getDao(Class<T> daoClass,HttpServletRequest req){
		WebApplicationContext ac =  WebApplicationContextUtils.getRequiredWebApplicationContext(req.getSession().getServletContext());
		
		String daoName = ArqUtils.getBeanName(daoClass);
		
		T dao = (T) ac.getBean(daoName);
		
		return dao;
	}
	
}
