package br.ufrn.sigpit.arq.seguranca.dominio;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import br.ufrn.sigpit.arq.dominio.DadosPessoais;
import br.ufrn.sigpit.arq.dominio.Persistente;


/**
 * Classe que identifica um usuario que pode se logar no sistema.
 * 
 * @author Mario Melo
 *
 */
@Entity
@Table(name = "usuario", schema = "public")
public class Usuario implements Persistente, UserDetails{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_usuario", sequenceName = "public.usuario_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_usuario")
	private int id;
	
	/**
	 * Indica o nome do usuario
	 */
	@NotNull(message = "Por favor, insira um nome.")
	private String nome;
	
	/**
	 * Indica a senha do usuario
	 */
	@NotNull(message = "Por favor, insira uma senha.")
	private String senha;
	
	/**
	 * Indica se o usuario esta ativo.
	 */
	private boolean ativo;
	
	/**
	 * Chave de confirmacao para ativacao da conta.
	 */
	private String chaveConfirmacao;
	
	/**
	 * Email do Usuario
	 */
	@NotNull(message = "Por favor, insira um email.")
	@Pattern(regexp = "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)", message = "O email não está no formato válido.")
	private String email;
	
	/**
	 * Indica as permissaes que um determinado usuario possua
	 */
	@OneToMany(mappedBy="usuario",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Permissao> permissoes;	

	@OneToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private DadosPessoais dadosPessoais;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Collection<Papel> papeis = new ArrayList<Papel>();
		
		for(Permissao p : permissoes){
			papeis.add(p.getPapel());
		}
		
		return papeis;
	}
	
	@Override
	public String getPassword() {
		return senha;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return email;
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return ativo;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public List<Permissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(List<Permissao> permissoes) {
		this.permissoes = permissoes;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public DadosPessoais getDadosPessoais() {
		return dadosPessoais;
	}
	
	public void setDadosPessoais( DadosPessoais dp) {
		dadosPessoais=dp;
	}

	public String getChaveConfirmacao() {
		return chaveConfirmacao;
	}

	public void setChaveConfirmacao(String cc) {
		chaveConfirmacao = cc;		
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
