package br.ufrn.sigpit.comum.enums;

public enum PapelUsuarioEnum {
	USUARIO("Usu�rio"),
	ADMINISTRADOR("Administrador"),
	SUPERADMINISTRADOR("Superadministrador");
	
	private final String label;
	
	private PapelUsuarioEnum(String label){
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
