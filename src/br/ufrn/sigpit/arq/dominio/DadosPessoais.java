package br.ufrn.sigpit.arq.dominio;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.ufrn.sigpit.arq.dominio.Persistente;

/***
 * 
 * @author Allyson Barros
 *
 */
@Entity
@Table(name="dados_pessoais", schema="public")
public class DadosPessoais implements Persistente {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "dados_pessoais_seq", sequenceName = "public.dados_pessoais_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="dados_pessoais_seq")
	private int id;
		
	@Temporal(TemporalType.DATE)
	private Date dataNascimento;
	
	private String sexo;
		
	@Column(name="orientador", columnDefinition="boolean default false")
	private boolean orientador;
	
	public DadosPessoais() {
	}

	public DadosPessoais(Date dataNascimento, String sexo) {
		this.dataNascimento = dataNascimento;
		this.sexo = sexo;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
		
	public Date getDataNascimento() {
		return dataNascimento;
	}
	
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	public String getSexo() {
		return sexo;
	}
	
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	
	public boolean isOrientador() {
		return orientador;
	}

	public void setOrientador(boolean orientador) {
		this.orientador = orientador;
	}
}
