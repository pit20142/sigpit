package com.exemplo.entidade;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * Classe responsável por mapear as colunas do banco
 * */

@Entity
@Table(name="turmas", schema="public")
public class MenssagemTurmas implements Comparable<MenssagemTurmas>{


	@Id 
	//@GeneratedValue(strategy = GenerationType.IDENTITY) 
	//@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name="\"idturma\"") 
	private String idturma;
	private Integer ano;
	private Integer semestre;
	private String nome;
	private String projeto;
	private String ativo;

	
	

	public String getProjeto() {
		return projeto;
	}
	public void setProjeto(String projeto) {
		this.projeto = projeto;
	}
	public String getAtivo() {
		return ativo;
	}
	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}
	public String getIdturma() {
		return idturma;
	}
	public void setIdturma(String idturma) {
		this.idturma = idturma;
	}
	public Integer getAno() {
		return ano;
	}
	public void setAno(Integer ano) {
		this.ano = ano;
	}
	public Integer getSemestre() {
		return semestre;
	}
	public void setSemestre(Integer semestre) {
		this.semestre = semestre;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	@Override
	public int compareTo(MenssagemTurmas o) {
		if(this.ano >= o.ano){
			if((this.ano==o.ano)&&(this.semestre>=o.semestre)){
				return 1;
			}else if((this.ano==o.ano)&&(this.semestre<o.semestre)){
				return -1;
			}
			return -1;

		}
		return 1;


	}

}
