package br.ufrn.sigpit.arq.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Classe que identifica as funcoes atribuidas aos usuarios do tipo aluno.
 * A identificacao do tipo do usuario sera determinada pelo seu papel.
 * 
 * @author Thiago Sena
 *
 */

@Entity
@Table(name="funcao", schema="public")
public class Funcao implements Persistente {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "funcao_seq", sequenceName = "public.funcao_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="funcao_seq")
	private int id;
	
	private String nome;

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNome(){
		return this.nome;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}
	
}
