/**
 * Universidade Federal do Rio Grande do Norte
 * Superintend�ncia de Inform�tica - UFRN
 * Diretoria de Sistemas
 *
 * Created on 05/06/2012
 *
 */
package br.ufrn.sigpit.comum.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Validator responsavel pela validacao do campo senha no cadastro dos dados de
 * acesso. A senha deve conter, obrigatoriamente, pelo menos uma letra e um numero em
 * qualquer ordem.
 * 
 * @author Argus Halley
 */
@FacesValidator(value = "br.ufrn.sigpit.comum.validators.PasswordValidator")
public class PasswordValidator implements Validator {

	private static int TAMANHO_MINIMO_SENHA = 6;
	private static int TAMANHO_MAXIMO_SENHA = 50;
	
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		String passwd = (String) value;
		
		Pattern pattern = Pattern
				.compile("(.*[A-Za-z].*[0-9].*)|(.*[0-9].*[A-Za-z].*)");
		Matcher matcher = pattern.matcher(passwd);

		if ((passwd != null && !matcher.find())||(passwd.length() < TAMANHO_MINIMO_SENHA)||(passwd.length() > TAMANHO_MAXIMO_SENHA)) {
			throw new ValidatorException(
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"A senha deve possuir letras e números e, no mínimo, 6 caracteres.",
							null));
		}
	}
}
