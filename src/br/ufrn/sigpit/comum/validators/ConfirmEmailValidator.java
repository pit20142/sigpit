/**
 * Universidade Federal do Rio Grande do Norte
 * Superintend�ncia de Inform�tica - UFRN
 * Diretoria de Sistemas
 *
 * Created on 29/05/2012
 *
 */
package br.ufrn.sigpit.comum.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Validator responsavel pela validacao dos campos senha e confirmacao da senha
 * no cadastro dos dados de acesso.
 * 
 * @author Argus Halley
 */

@FacesValidator(value = "br.ufrn.sigpit.comum.validators.ConfirmEmailValidator")
public class ConfirmEmailValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		UIInput emailComponent = (UIInput) component.getAttributes().get(
				"emailComponent");
		String email = (String) emailComponent.getValue();
		String confirmEmail= (String) value;

		if (confirmEmail != null && !confirmEmail.equals(email)) {
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"Email e confirma��o n�o conferem", null));
		}
	}
}
