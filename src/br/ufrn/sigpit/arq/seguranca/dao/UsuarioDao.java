package br.ufrn.sigpit.arq.seguranca.dao;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.ufrn.sigpit.arq.dao.GenericDao;
import br.ufrn.sigpit.arq.exceptions.DAOException;
import br.ufrn.sigpit.arq.seguranca.dominio.Usuario;

/**
 * Classe centralizadora das consultas envolvendo o usuario.
 * 
 * @author Mario Melo
 *
 */
@Repository
public class UsuarioDao extends GenericDao {
	
	@Autowired
	public UsuarioDao(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
	}
	
	/**
	 * Metodo utilizado para pesquisar um usuario a partir de login e senha.
	 * 
	 * @param login
	 * @param senha
	 * @return
	 * @throws DAOException
	 */
	public Usuario findUsuarioByLoginSenha(String login, String senha) throws DAOException{
		Criteria c = getCriteria(Usuario.class);
		
		c.add(Restrictions.eq("login",login));
		c.add(Restrictions.eq("senha",senha));
		
		return (Usuario) c.uniqueResult();
	}

	/**
	 * Metodo utilizado para buscar o usuario pela chave de confirmacao
	 * 
	 * @param chave
	 * @return
	 */
	public Usuario findUsuarioByChaveConfirmacao(String chave){
		Criteria c = getCriteria(Usuario.class);
		
		c.add(Restrictions.eq("chaveConfirmacao", chave));
		c.add(Restrictions.eq("ativo", false));
		
		return (Usuario) c.uniqueResult();
		
	}
	
	/**
	 * Metodo utilizado para buscar o usuario pela chave de confirmacao
	 * 
	 * @param chave
	 * @return
	 */
	public Usuario findUsuarioAtivoByChaveConfirmacao(String chave){
		Criteria c = getCriteria(Usuario.class);
		
		c.add(Restrictions.eq("chaveConfirmacao", chave));
		c.add(Restrictions.eq("ativo", true));
		
		Usuario user = (Usuario) c.uniqueResult();
		System.out.println("findUsuarioAtivoByChaveConfirmacao: " + user.getEmail());
		return user;
		
	}
	
	/**
	 * Metodo utilizado para verificar se a chave de confirmacao informada existe na base de dados.
	 * 
	 * @param chave
	 * @return
	 */
	public boolean isChaveCadastrada(String chave) {
		if (findUsuarioByChaveConfirmacao(chave) != null){
			return true;
		}
		
		return false;
	}

	/**
	 * Metodo utilizado para buscar o usuario pelo email
	 * 
	 * @param chave
	 * @return
	 */
	public Usuario findUsuarioByEmail(String email){
		
		Criteria c = getCriteria(Usuario.class);
		
		c.add(Restrictions.like("email", email));
		
		return (Usuario) c.uniqueResult();
		
	}
	
	/**
	 * Metodo utilizado para verificar se email informado existe na base de dados.
	 * 
	 * @param chave
	 * @return
	 */
	public boolean isEmailJaCadastrado(String email) {
		if (findUsuarioByEmail(email) != null){
			return true;
		}
		
		return false;
	}	
	
	public List<Usuario> getUsuarios(){
		return null;
		
	}
}
