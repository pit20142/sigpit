/**
 * Universidade Federal do Rio Grande do Norte
 * Superintend�ncia de Inform�tica da Universidade Federal do Rio Grande do Norte (SINFO)
 * 
 * Programa de Resid�ncia em Software da SINFO
 * iTConnect - Rede Social de Neg�cios
 * 
 * Autor: Wallace Medeiros
 * Criado em: 06/06/2012
 */
package br.ufrn.sigpit.arq.seguranca.dominio;

/**
 * Classe que agrega constantes com todos os papeis do sistema.
 * 
 * @author Wallace Medeiros
 *
 */
public class PapeisSistema {
	
	/**
	 * TODO - Commentar cada papel mais tarde.
	 */
	
	public static final String ADMINISTRADOR = "ROLE_ADMIN";
	
	public static final String GERENTE = "ROLE_GERENTE";
	
	public static final String PROFISIONAL = "ROLE_PROFISSIONAL";
	
}
