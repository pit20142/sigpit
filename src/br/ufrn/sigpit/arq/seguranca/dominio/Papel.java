package br.ufrn.sigpit.arq.seguranca.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

import br.ufrn.sigpit.arq.dominio.Persistente;

/**
 * Indica o papel presente no sistema.
 * 
 * @author Mario Melo
 *
 */
@Entity
@Table(name = "papel", schema = "public")
public class Papel implements Persistente,GrantedAuthority{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador da entidade
	 */
	@Id
	@SequenceGenerator(name = "seq_papel", sequenceName = "public.papel_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_papel")
	private int id;
	
	/**
	 * Nome do papel
	 */
	private String nome;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String getAuthority() {
		return nome;
	}


}
