/**
 * Universidade Federal do Rio Grande do Norte
 * Superintend�ncia de Inform�tica - UFRN
 * Diretoria de Sistemas
 *
 * Created on 05/06/2012
 *
 */
package br.ufrn.sigpit.comum.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Validator responsavel pela validacao do campo login no cadastro dos dados de
 * acesso. O login deve possuir apenas letras e numeros alem de iniciar com uma letra.
 * 
 * @author Argus Halley
 */
@FacesValidator(value = "br.ufrn.sigpit.comum.validators.LoginValidator")
public class LoginValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		String login = (String) value;
		Pattern pattern = Pattern.compile("^[A-Za-z][A-Za-z0-9]+$");
		Matcher matcher = pattern.matcher(login);

		if (login != null && !matcher.find()) {
			throw new ValidatorException(
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"o login deve possuir apenas letras e números, e iniciar com uma letra",
							null));
		}
	}
}
