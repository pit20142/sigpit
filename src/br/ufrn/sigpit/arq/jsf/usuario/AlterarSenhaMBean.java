package br.ufrn.sigpit.arq.jsf.usuario;

import java.util.Date;
import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.servlet.http.HttpServletRequest;

import br.ufrn.sigpit.arq.jsf.AbstractMBean;
import br.ufrn.sigpit.arq.seguranca.dao.UsuarioDao;
import br.ufrn.sigpit.arq.seguranca.dominio.Usuario;
import br.ufrn.sigpit.arq.service.ServiceCadastro;
import br.ufrn.sigpit.comum.helpers.AutenticacaoHelper;

@ManagedBean
@RequestScoped
public class AlterarSenhaMBean extends AbstractMBean<Usuario> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManagedProperty(value="#{serviceCadastro}")
	private ServiceCadastro serviceCadastro;
	
	private String senhaAtual;
	private String novaSenha;
	private String confirmacaoNovaSenha;
	private String chaveConfirmacao;
	
	private UIInput inputSenhaAtual;
	private UIInput inputNovaSenha;
	private UIInput inputNovaSenhaConfirmacao;
	
	// Atributos para o Caso de Uso: Esqueci minha Senha
	private String email;
	private UIInput inputEmail;
	
	public AlterarSenhaMBean() {
		this.senhaAtual = "";
		this.novaSenha = "";
		this.confirmacaoNovaSenha = "";
		this.chaveConfirmacao = getParameter("chave");
		
		this.inputEmail = new UIInput();
		this.inputSenhaAtual = new UIInput();
		this.inputNovaSenha = new UIInput();
		this.inputNovaSenhaConfirmacao = new UIInput();
	}
	
	public String alterarSenha() {
		String senhaCriptografada = AutenticacaoHelper.toMD5(senhaAtual);
		
		if (senhaCriptografada.equals(getUsuarioLogado().getSenha())) {
			UsuarioDao dao = (UsuarioDao) getBean("usuarioDao");
			obj = dao.buscar(Usuario.class, getUsuarioLogado().getId());
			
			String novaSenhaCriptografada = AutenticacaoHelper.toMD5(novaSenha);
			obj.setSenha(novaSenhaCriptografada);
			serviceCadastro.atualizar(obj);

			//ServiceLogon s = new ServiceLogon();
			//s.logout();

			addFacesInformationMessage("Sua senha foi alterada com sucesso!");
		} else {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "a senha atual n�o confere. ", null);     
			FacesContext.getCurrentInstance().addMessage(inputSenhaAtual.getClientId(), message);  
			inputSenhaAtual.setValid(false);
			
			return null;
		}
			
		return "index.jsf";
	}
	
	public String enviarEmailRedefinicaoSenha() throws MessagingException {
		if(email == null || email.equals("")) {
			addFacesErrorMessage("Informe o Email.");
			return "/login.jsf";
		}
		
		UsuarioDao dao = (UsuarioDao) getBean("usuarioDao");
		obj = dao.findUsuarioByEmail(email);
		
		if (obj != null) {
			Properties props = new Properties();  
			  props.put("mail.smtp.host","mail.info.ufrn.br");
			  props.put("mail.smtp.port", "25"); 
			 
			  Session session = Session.getDefaultInstance(props);  
			 
			  MimeMessage message = new MimeMessage(session);  	  
			  
			  Address from = new InternetAddress("ticonnectsinfo@gmail.com");  
			  Address to = new InternetAddress(obj.getEmail());  
			   
			  message.setFrom(from);  
			  message.addRecipient(RecipientType.TO, to);  
			  
			  message.setSentDate(new Date());  
			  message.setSubject("Solita��o de Redefini��o de Senha - TIConnect");  
			  
			  //Obtendo nome, porta e contexto do servidor
			  String enderecoServidor,porta, contexto;
			  Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();

			  enderecoServidor = ((HttpServletRequest) request).getLocalName().toString();
			  contexto = ((HttpServletRequest) request).getContextPath().toString();
			  porta = Integer.toString(((HttpServletRequest) request).getServerPort());

			  
			  String html = "<p><h2>Olá "+obj.getNome()+", </h2></p> ";
			  html += "<p><h4>Atenção: você solicitou a redefinição da sua senha. Para efetuar a mudança de sua senha clique <a href='http://"+enderecoServidor+":"+porta+contexto+"/alterarSenha.jsf?chave="+obj.getChaveConfirmacao()+"'>AQUI</a>.</h4></p>";
			  html += "<p></br><h4>Atenciosamente,</h4></p>";
			  html += "<p><h4>TIConnect - ligando as suas habilidades ao mercado</h4></p>";
			  
			  message.setText(html,"ISO-8859-1", "html");  
			  Transport.send(message);
		} else {
			addFacesErrorMessage("Nenhum usuário cadastrado com o email informado.");
			return null;
		}
		
		addFacesInformationMessage(String.format("Email para redefinição da senha enviado com sucesso. Verifique seu email %s.\n  Se o email não estiver em sua caixa de entrada, verifique sua caixa de spam!", obj.getEmail()));
		return "login.jsf";
	}
	
	public String getNovaSenha() {
		return novaSenha;
	}
	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}
	public UIInput getInputNovaSenha() {
		return inputNovaSenha;
	}
	public void setInputNovaSenha(UIInput inputNovaSenha) {
		this.inputNovaSenha = inputNovaSenha;
	}
	public UIInput getinputNovaSenhaConfirmacao() {
		return inputNovaSenhaConfirmacao;
	}
	public void setinputNovaSenhaConfirmacao(UIInput inputNovaSenhaConfirmacao) {
		this.inputNovaSenhaConfirmacao = inputNovaSenhaConfirmacao;
	}

	public String getSenhaAtual() {
		return senhaAtual;
	}

	public void setSenhaAtual(String senhaAtual) {
		this.senhaAtual = senhaAtual;
	}

	public String getConfirmacaoNovaSenha() {
		return confirmacaoNovaSenha;
	}

	public void setConfirmacaoNovaSenha(String confirmacaoNovaSenha) {
		this.confirmacaoNovaSenha = confirmacaoNovaSenha;
	}

	public UIInput getInputSenhaAtual() {
		return inputSenhaAtual;
	}

	public void setInputSenhaAtual(UIInput inputSenhaAtual) {
		this.inputSenhaAtual = inputSenhaAtual;
	}

	public void setServiceCadastro(ServiceCadastro serviceCadastro) {
		this.serviceCadastro = serviceCadastro;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UIInput getInputEmail() {
		return inputEmail;
	}

	public void setInputEmail(UIInput inputEmail) {
		this.inputEmail = inputEmail;
	}

	public String getChaveConfirmacao() {
		return chaveConfirmacao;
	}

	public void setChaveConfirmacao(String chaveConfirmacao) {
		this.chaveConfirmacao = chaveConfirmacao;
	}
}
