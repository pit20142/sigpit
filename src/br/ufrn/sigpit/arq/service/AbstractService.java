package br.ufrn.sigpit.arq.service;

import org.springframework.security.core.context.SecurityContextHolder;

import br.ufrn.sigpit.arq.seguranca.dominio.Usuario;

/**
 * Service Basico que deve ser herado por todos os services do sistema.
 * 
 * @author Mario Melo
 *
 */
public abstract class AbstractService {
	
	public Usuario getUsuarioLogado(){
		if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof Usuario)
			return (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		return null;
	}
}
