package br.ufrn.sigpit.comum.helpers;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class AutenticacaoHelper {
	/**
	 * Transforma a String passada em MD5.
	 * 
	 * @param senha
	 * @return
	 */
	public static String toMD5(String senha) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");

			BigInteger hash = new BigInteger(1, md.digest(senha.getBytes()));
			String s = hash.toString(16);

			while (s.length() < 32)
				s = "0" + s;

			return s;

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String toMD52(String senha) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		md.update(senha.getBytes());
		byte[] hashMd5 = md.digest();

		System.out.println("call to md5: \n" + "Entrada: " + senha
				+ "\nSa�da: " + new String(hashMd5));
		return new String(hashMd5);

	}

	public static String toMD53(String senha) {
		String sen = "";
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		BigInteger hash = new BigInteger(1, md.digest(senha.getBytes()));
		sen = hash.toString(16);
		return sen;
	}
	
	public static String gerarChaveConfirmacao(){
			String[] carct ={"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"}; 

			String chaveConfirm=""; 

			for (int x=0; x<10; x++){ 
				int j = (int) (Math.random()*carct.length); 
				chaveConfirm += carct[j]; 
			} 

			System.out.println("A SENHA GERADA É: "+chaveConfirm); 
		return chaveConfirm;
	}
}
