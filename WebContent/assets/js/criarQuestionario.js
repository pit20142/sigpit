/* <![CDATA[ */
		var id_count = 0;
		
			function removerCampo(id)
			{ 
			  $("#"+id).remove();
			}
			function adicionarOpcao(contador, divAfter, divAtual, tipo){
				var opcao = "<tr id='opcoes"+divAtual+""+contador+"'>"+
									  "<td></td>"+
									  "<td></td>"+
									  "<td>"+
									    "<td>"+
										"<label class='"+tipo+"'>"+
										  "<input type='"+tipo+"' name='optionsRadios' id='optionsRadios' value='option2' disabled='disabled' style='cursor: pointer'>"+
										  "</input>"+
										  "<input class='input-large' type='text' placeholder='Opcao "+window[contador]+"'></input>"+										  
										"</label>"+
										"</td>"+
										"<td>"+
											"<button class='close' onclick='removerCampo(\"opcoes"+divAtual+""+contador+"\")'><i class='icon-remove'></i></button>"+
										"</td>"+
									  "</td>"+
									  "<td></td>"+
									"</tr>";
				window[contador]++;
				$("#"+divAfter).before(opcao);
			}
			function adicionarOutro(contador, divBefore, divAtual, tipo){
				var opcao = "<tr id='opcoes"+divAtual+""+contador+"'>"+
									  "<td></td>"+
									  "<td></td>"+
									  "<td >"+
									    "<td>"+
										"<label class='"+tipo+"'>"+
										  "<input type='"+tipo+"' name='optionsRadios' id='optionsRadios2' value='option2' disabled='disabled' style='cursor: pointer'>'Outro: '</input>"+
										  "Outro: "+
										  "<input class='input-large' type='text' placeholder='Resposta do usuário' disabled='disabled' style='cursor: pointer'></input>"+										  
										"</label>"+
										"</td>"+
										"<td>"+
											"<button class='close' onclick='removerCampo(\"opcoes"+divAtual+""+contador+"\")'><i class='icon-remove'></i></button>"+
										"</td>"+
									  "</td>"+
									  "<td></td>"+
									"</tr>";
				window[contador]++;
				$("#"+divBefore).after(opcao);
			}
			var camposPadrao = "<div class='well' id='div"+id_count+"'>"+
							  "<table>"+								  
								  "<tbody>"+
									"<tr>"+
									  "<td><label class='control-label' for='perguntaTexto"+id_count+"'>Titulo da pergunta</label></td>"+
									  "<td width='30'></td>"+
									  "<td><input id='perguntaTexto"+id_count+"' class='input-xxlarge' type='text' placeholder='Pergunta sem título'></input></td>"+
									  "<td></td>"+
									  "<td width='25%'><button class='close' onclick='removerCampo(\"div"+id_count+"\")'><i class='icon-trash'></i></button></td>"+
									"</tr>"+
									"<tr id='ajudaLinha"+id_count+"'>"+
									  "<td><label class='control-label' for='ajudaTexto"+id_count+"'>Texto de ajuda</label></td>"+
									  "<td height='5%'></td>"+
									  "<td><input id='ajudaTexto"+id_count+"' class='input-xxlarge' type='text'></input></td>"+
									  "<td><button class='close' onclick='removerCampo(\"ajudaLinha"+id_count+"\")'><i class='icon-remove'></i></button></td>"+
									"</tr>"+
									"<tr>"+									 					
									"</tr>";
		$(document).ready(function(){
			/*Adicionando pergunta de texto*/
		  $("#adcText").click(function(){
			var divTexto2 = 	""+
								  "</tbody>"+
								"</table>"+
								"<br></br><div >"+
									"<td ><input style='border: 1px dashed #0099CC' class='input-large' type='text' placeholder='Resposta deles' disabled='disabled'></input></td>"+
								"</div>"+
							"</div>";
			$("#accordion2").append(camposPadrao+divTexto2);		
			id_count = id_count + 1;
		  });
		  /*Adicionando pergunta de caixa de texto*/
		  $("#adcCaixaText").click(function(){
			var divTexto2 = 		""+						
								  "</tbody>"+
								"</table>"+
								"<br></br><div >"+
									"<td ><textarea rows='5' class='textarea-xxlarge' type='text' placeholder='Resposta mais longa deles' style='border: 1px dashed #0099CC' disabled='disabled'></textarea></td>"+
								"</div>"+
							"</div>";
			$("#accordion2").append(camposPadrao+divTexto2);		
			id_count = id_count + 1;
		  });
		  /*Adicionando pergunta de radiobutton*/
		  $("#adcMultEsc").click(function(){
			var contador = 'opcoesContador'+id_count;
			var divAtual = id_count;
			window[contador]=2;			
			var divTexto2 = ""+
									"<tr><br></br></tr>"+
									//Opção default								
									"<tr id='opcoes"+id_count+"1'>"+
									  "<td></td>"+
									  "<td></td>"+
									  "<td>"+
									    "<td>"+
										"<label class='radio'>"+
										  "<input type='radio' name='optionsRadios' id='optionsRadios' value='option2' disabled='disabled'>"+
										  "</input>"+
										  "<input class='input-large' type='text' placeholder='Opção 1'></input>"+										  
										"</label>"+
										"</td>"+
										"<td>"+
											"<button class='close' onclick='removerCampo(\"opcoes"+id_count+"1\")'><i class='icon-remove'></i></button>"+
										"</td>"+
									  "</td>"+
									  "<td></td>"+
									"</tr>"+
									//Opção outro - adicionar opção
									"<tr id='linhaOutro"+id_count+"'>"+
									  "<td></td>"+
									  "<td></td>"+
									  "<td>"+
										"<td>"+
											"<label class='radio' onclick='adicionarOpcao(\""+contador+"\",\"linhaOutro"+id_count+"\",\""+divAtual+"\",\"radio\")'>"+
											  "<input type='radio' name='optionsRadios' id='optionsRadios' value='option2' disabled='disabled' style='cursor: pointer'></input>"+										  
											  "<input class='input-large' type='text' placeholder='Clique para adicionar uma opção' disabled='disabled' style='cursor: pointer'></input>"+
											"</label>"+
										"</td>"+
										"<td>"+
											//"ou "+"<button class='btn btn link' type='button' onclick='adicionarOutro(\""+contador+"\",\"linhaOutro"+id_count+"\",\""+divAtual+"\",\"radio\")'>Adicionar \"Outro\"</button>"+
										"</td>"+
									  "</td>"+
									  "<td></td>"+
									"</tr>"+
								  "</tbody>"+
								"</table>"+
								"<br></br>"+
						"</div>";
			$("#accordion2").append(camposPadrao+divTexto2);	
			id_count = id_count + 1;
		  });
		  /*Adicionando pergunta de checkbox*/
		  $("#adcCheck").click(function(){
			var contador = 'opcoesContador'+id_count;
			var divAtual = id_count;
			window[contador]=2;			
			var divTexto2 = ""+
									"<tr><br></br></tr>"+
									//Opção default								
									"<tr id='opcoes"+id_count+"1'>"+
									  "<td></td>"+
									  "<td></td>"+
									  "<td>"+
									    "<td>"+
										"<label class='checkbox'>"+
										  "<input type='checkbox' name='optionsRadios' id='optionsRadios' value='option2' disabled='disabled'>"+
										  "</input>"+
										  "<input class='input-large' type='text' placeholder='Opção 1'></input>"+										  
										"</label>"+
										"</td>"+
										"<td>"+
											"<button class='close' onclick='removerCampo(\"opcoes"+id_count+"1\")'><i class='icon-remove'></i></button>"+
										"</td>"+
									  "</td>"+
									  "<td></td>"+
									"</tr>"+
									//Opção outro - adicionar opção
									"<tr id='linhaOutro"+id_count+"'>"+
									  "<td></td>"+
									  "<td></td>"+
									  "<td>"+
										"<label class='radio' onclick='adicionarOpcao(\""+contador+"\",\"linhaOutro"+id_count+"\",\""+divAtual+"\",\"checkbox\")'>"+
										  "<input type='radio' name='optionsRadios' id='optionsRadios' value='option2' disabled='disabled' style='cursor: pointer'></input>"+										  
										  "<input class='input-large' type='text' placeholder='Clique para adicionar uma opção' disabled='disabled' style='cursor: pointer'></input>"+
										"</label>"+										
									  "</td>"+
									  "<td></td>"+
									"</tr>"+
								  "</tbody>"+
								"</table>"+
								"<br></br>"+
						"</div>";
			$("#accordion2").append(camposPadrao+divTexto2);	
			id_count = id_count + 1;
		  });
		});
		/* ]]> */