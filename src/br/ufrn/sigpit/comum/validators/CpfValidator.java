package br.ufrn.sigpit.comum.validators;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Valida CPF's
 * 
 * @author Rayron Victor
 *
 */
@FacesValidator(value = "br.ufrn.sigpit.comum.validators.CpfValidator")
public class CpfValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component, Object value)
			throws ValidatorException {
		
		String cpf = (String) value;
		cpf = cpf.replace(".", "").replace("-", "");
		
		Boolean valido = false;
		
		if(cpf.length() == 11){
			Integer v1 = 0, v2 = 0;
			ArrayList<Integer> ns = new ArrayList<Integer>();
			
			//Transforma em um array de inteiros
			for(int i = 0; i < cpf.length(); i++){
				ns.add(Integer.parseInt(cpf.substring(i, i+1)));
			}
			
			//Calcula o 1e digito verificador
			v1 += 10*ns.get(0) + 9*ns.get(1) + 8*ns.get(2);
			v1 +=  7*ns.get(3) + 6*ns.get(4) + 5*ns.get(5);
			v1 +=  4*ns.get(6) + 3*ns.get(7) + 2*ns.get(8);
			int resto = v1 % 11;
			v1 = (11 - resto < 10 ? 11 - resto : 0);
			
			//Calcula o segundo digito verificador
			v2 += 11*ns.get(0) + 10*ns.get(1) + 9*ns.get(2);
			v2 +=  8*ns.get(3) +  7*ns.get(4) + 6*ns.get(5);
			v2 +=  5*ns.get(6) +  4*ns.get(7) + 3*ns.get(8);
			v2 +=  2*v1;
			resto = v2 % 11;
			v2 = (11 - resto < 10 ? 11 - resto : 0);
			
			if(v1 == ns.get(9) && v2 == ns.get(10)){
				valido = true;
			}
		}
		
		if(!valido){
			throw new ValidatorException(
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"CPF inválido",
							null));
		}
		
		
	}

}
