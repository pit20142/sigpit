package br.ufrn.sigpit.arq.seguranca.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.ufrn.sigpit.arq.dominio.Persistente;

/**
 * Classe responsavel por fazer a indicacao a qual permissao o usuario possui
 * 
 * @author Mario
 *
 */
@Entity
@Table(name = "permissao", schema = "public")
public class Permissao implements Persistente{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificador da entidade.
	 */
	@Id
	@SequenceGenerator(name = "permissao_seq", sequenceName = "public.permissao_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "permissao_seq")
	private int id;
	
	/**
	 * Indica o papel a que o usuario tem permissao.
	 */
	@ManyToOne
	@JoinColumn(name="id_papel")
	private Papel papel;
	
	/**
	 * Indica a usuario que possui determinada permissao.
	 */
	@ManyToOne
	@JoinColumn(name="id_usuario")
	private Usuario usuario;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Papel getPapel() {
		return papel;
	}

	public void setPapel(Papel papel) {
		this.papel = papel;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
