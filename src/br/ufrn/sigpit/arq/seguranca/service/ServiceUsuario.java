package br.ufrn.sigpit.arq.seguranca.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.ufrn.sigpit.arq.seguranca.dao.UsuarioDao;
import br.ufrn.sigpit.arq.seguranca.dominio.Usuario;
import br.ufrn.sigpit.arq.service.AbstractService;

/**
 * Service responsavel por realizar todas as operacoes sobre o usuario
 * 
 * @author Mario Melo
 *
 */
@Service
public class ServiceUsuario extends AbstractService implements UserDetailsService{
	
	/**
	 * Indica o dao de usuario
	 */
	@Autowired
	private UsuarioDao usuarioDao;
	
	/**
	 * Metodo utilizado pelo Spring Security para realizar a operacao de login no sistema.
	 */
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Usuario usuario = usuarioDao.findUsuarioByEmail(email);

		if(usuario == null)
			throw new UsernameNotFoundException("O usuario informado nao existe.");

		return usuario;
	}
}
