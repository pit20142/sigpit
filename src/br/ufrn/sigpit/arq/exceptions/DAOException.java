package br.ufrn.sigpit.arq.exceptions;

public class DAOException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public DAOException(Exception e) {
		super(e);
	}

	public DAOException(String msg) {
		super(msg);		
	}

	public DAOException() {
		super("Erro de Daos");		
	}

}
