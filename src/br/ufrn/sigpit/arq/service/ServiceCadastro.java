package br.ufrn.sigpit.arq.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufrn.sigpit.arq.dao.GenericDao;
import br.ufrn.sigpit.arq.dominio.Persistente;
import br.ufrn.sigpit.arq.exceptions.DAOException;


@Service("serviceCadastro")
public class ServiceCadastro extends AbstractService{
	
	@Autowired
	@Qualifier("genericDao")
	protected GenericDao dao;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Transactional(readOnly=false)
    public Object getById(final Class classe, final int id) throws DAOException{
		return dao.buscar(classe, id);
    }
	

	@Transactional(readOnly=false)
    public void salvar(Persistente objeto) throws DAOException{
    	dao.salvar(objeto);

    }

	@Transactional(readOnly=false)
	public void atualizar(Persistente objeto) throws DAOException{
    	dao.atualizar(objeto);
    }
    

	@Transactional(readOnly=false)
	public void excluir(Persistente objeto) throws DAOException{		
		dao.excluir(objeto);		
    }

}
