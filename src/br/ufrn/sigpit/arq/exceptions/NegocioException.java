package br.ufrn.sigpit.arq.exceptions;

public class NegocioException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public NegocioException(Exception e) {
		super(e);
	}

	public NegocioException(String msg) {
		super(msg);		
	}

	public NegocioException() {
		super("Erro de Neg�cio");		
	}

}