package br.ufrn.sigpit.arq.jsf;

import java.util.Date;
import java.util.Properties;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;

import br.ufrn.sigpit.arq.jsf.AbstractMBean;
import br.ufrn.sigpit.arq.seguranca.dao.UsuarioDao;
import br.ufrn.sigpit.arq.seguranca.dominio.PapeisSistema;
import br.ufrn.sigpit.arq.seguranca.dominio.Usuario;
import br.ufrn.sigpit.arq.seguranca.service.ServiceLogon;


@ManagedBean
@RequestScoped
public class LoginMBean extends AbstractMBean<Usuario>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManagedProperty(value="#{serviceLogon}")
	private ServiceLogon serviceLogon;

	private boolean logado;
	private boolean papelGerente;
	private boolean papelProfissional;

	public LoginMBean(){
		obj = new Usuario();
	}

	public String logar() throws Exception{
		System.out.println("Adson: Cheguei no logar!");
		if((obj.getEmail() == null || obj.getEmail().trim().equals(""))
				|| (obj.getSenha() == null || obj.getSenha().trim().equals(""))){
			addFacesErrorMessage("Informe Login e Senha.");
			addFacesErrorMessage(obj.getEmail());
			return "/login.jsf";
		}
		
		obj.setEmail(obj.getEmail().toLowerCase());

		try{
			Usuario user = serviceLogon.login(obj);
			obj = user;
			System.out.println("Objeto recebeu usuario!");
			return "/pages/index.jsf";

		} catch(Exception e){
			e.printStackTrace();
			if(e instanceof DisabledException){
				
				addFacesInformationMessage("Caso não tenha recebido o email de ativação, clique <a href='" + getCurrentRequest().getContextPath() + "/reenviarEmailAtivacao.jsf'>AQUI</a>." );
				addFacesErrorMessage("Usuário não está habilitado a entrar no sistema. " +
						"Ative já a sua conta através do email cadastrado.");
			}else{
				addFacesErrorMessage("Usuário não encontrado para o email e senha informados.");
			}
			return "/login.jsf";
		}
	}

	public String deslogar() {
		try{
			serviceLogon.logout();
			obj = null;
			addFacesInformationMessage("Você se desconectou do SiGPIT.");

		}catch(BadCredentialsException e){
			addFacesErrorMessage("O Usuário já foi deslogado do sistema.");
		}

		return "/login?faces-redirect=true";
	}

	public String reenviarEmailAtivacao() throws MessagingException {
		if(obj.getEmail() == null || obj.getEmail().trim().equals("")) {
			addFacesErrorMessage("Informe o Email.");
			return "/login.jsf";
		} 
		
		UsuarioDao dao = (UsuarioDao) getBean("usuarioDao");
		obj = dao.findUsuarioByEmail(obj.getEmail());
		
		if (obj.isAtivo()) {
			addFacesInformationMessage("ATENÇÃO: Sua conta já foi ativada anteriormente.");
			return "/login.jsf";
		}
		
		Properties props = new Properties();  
		props.put("mail.smtp.host","mail.info.ufrn.br");
		props.put("mail.smtp.port", "25"); 

		Session session = Session.getDefaultInstance(props);  

		MimeMessage message = new MimeMessage(session);  	  

		Address from = new InternetAddress("ticonnectsinfo@gmail.com");  
		Address to = new InternetAddress(obj.getEmail());  

		message.setFrom(from);  
		message.addRecipient(RecipientType.TO, to);  

		message.setSentDate(new Date());  
		message.setSubject("Confirmação de Cadastro no TIConnect");  

		//Obtendo nome, porta e contexto do servidor
		String enderecoServidor,porta, contexto;
		Object request = FacesContext.getCurrentInstance().getExternalContext().getRequest();

		enderecoServidor = ((HttpServletRequest) request).getLocalName().toString();
		contexto = ((HttpServletRequest) request).getContextPath().toString();
		porta = Integer.toString(((HttpServletRequest) request).getServerPort());


		String html = "<p><h2>Olá "+obj.getNome()+", </h2></p> ";
		html += "<p><h4>Seja bem-vindo(a) ao SIGS!</h4></p>";
		html += "<p><h4>Atenção: sua conta encontra-se desativada. Ative já a sua conta clicando <a href='http://"+enderecoServidor+":"+porta+contexto+"/login.jsf?chave="+obj.getChaveConfirmacao()+"'>AQUI</a>.</h4></p>";
		html += "<p></br><h4>Atenciosamente,</h4></p>";
		html += "<p><h4>SiGPIT - Sistema de Gerenciamento do Programa de Imerssão Tecnológica</h4></p>";
		  
		message.setText(html,"ISO-8859-1", "html");  

		Transport.send(message);
		
		addFacesInformationMessage(String.format("Email para ativação da conta enviado com sucesso. Verifique seu email %s.\n  Se o email não estiver em sua caixa de entrada, verifique sua caixa de spam!", obj.getEmail()));
		return "/login.jsf";
	}

	public void setServiceLogon(ServiceLogon serviceLogon) {
		this.serviceLogon = serviceLogon;
	}
	
	public Boolean hasRole(String ... roles){
		SecurityContextHolderAwareRequestWrapper sec = new SecurityContextHolderAwareRequestWrapper(getCurrentRequest(), "ROLE_");
		for(String role : roles) if(sec.isUserInRole(role)) return true;
		return false;
	}

	public boolean isPapelAdministrador() {
		boolean papelAdministrador;
		try{
			checkRole(PapeisSistema.ADMINISTRADOR);
			papelAdministrador = true;
		} catch (Exception e) {
			papelAdministrador = false;
		} 
		return papelAdministrador;
	}

	public boolean isPapelGerente() {
		try{
			checkRole(new String[]{"ROLE_GERENTE"});
			papelGerente = true;
		} catch (Exception e) {
			papelGerente = false;
		} 
		return papelGerente;
	}

	public boolean isPapelProfissional() {
		try{
			checkRole(new String[]{"ROLE_PROFISSIONAL"});
			papelProfissional = true;
		} catch (Exception e) {
			papelProfissional = false;
		} 
		return papelProfissional;
	}

	public boolean isLogado(){
		logado = false;
		if (getUsuarioLogado() != null) logado = true;		
		return logado;
	}

}
